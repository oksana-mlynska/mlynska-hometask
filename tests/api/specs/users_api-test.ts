import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { PostsController } from "../lib/controllers/posts.controller";
import { test } from "mocha";
import { createDocumentRegistry } from "typescript";

var chai = require("chai");
chai.use(require("chai-json-schema"));

const posts = new PostsController();
const schemas = require("./data/schemas_testData.json");
const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

let testUserId: BigInt;
let accessToken: string;
let bearerToken: string;
let postID: BigInt;

// variables that contain data for testing
let testEmail = global.appConfig.users.TestUser.email;
let testPass = global.appConfig.users.TestUser.password;
let regName = "Maria";
let changeName = "Marta";
let invalidEmailDataSet = [
    { email: "", password: testPass },
    { email: "", password: "" },
    { email: "wrong@email.com", password: testPass },
    { email: `i${testEmail}`, password: testPass },
];
let invalidPassDataSet = [
    { email: testEmail, password: "" },
    { email: testEmail, password: "wrongPaass" },
    { email: testEmail, password: `j${testPass}` },
];

describe(`Testing users modifications`, () => {
    before(`Checking the environment and register new user`, async () => {
        console.log("We are using " + global.appConfig.envName);

        let response = await register.registration(testEmail, testPass, regName);
        bearerToken = response.body.token.accessToken.token;

        expect(response.body).to.be.jsonSchema(schemas.schema_oneUserWithToken);
        checkStatusCode(response, 201);
        checkResponseTime(response, 5000);
    });

    it(`Get all users`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 10000);
        expect(response.body.length, `The amount of users is more than 0`).to.be.greaterThan(0);
        // expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);
    });

    it(`Logging in with valid credentials`, async () => {
        let response = await auth.login(testEmail, testPass, bearerToken);
        testUserId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_oneUserWithToken);
    });

    invalidEmailDataSet.forEach((credentials) => {
        it(`Logging in with invalid e-mail`, async () => {
            let response = await auth.login(credentials.email, credentials.password, bearerToken);

            checkStatusCode(response, 404);
            checkResponseTime(response, 5000);
        });
    });

    invalidPassDataSet.forEach((credentials) => {
        it(`Logging in with valid e-mail and invalid password`, async () => {
            let response = await auth.login(credentials.email, credentials.password, bearerToken);

            checkStatusCode(response, 401);
            checkResponseTime(response, 5000);
        });
    });

    it(`Get user from Token`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_oneUser);
        expect(response.body.email).to.equal(testEmail);
    });

    it(`Update user`, async () => {
        let userData: object = {
            id: testUserId,
            avatar: "avatar",
            email: testEmail,
            userName: changeName,
        };
        let response = await users.updateUser(userData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 5000);
    });

    it(`Get user by ID and verify changes`, async () => {
        let response = await users.getUserById(testUserId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_oneUser);
        expect(response.body.userName).to.equal(changeName);
        expect(response.body.email).to.equal(testEmail);
    });
});

describe(`Testing posts feature`, () => {
    it(`Getting all posts`, async () => {
        let response = await posts.getAllPosts();
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body.length, `The amount of posts is more than 0`).to.be.greaterThan(0);
        postID = response.body[23].id;
    });

    it(`Add like to post`, async () => {
        let postData: object = {
            entityId: postID,
            isLike: true,
            userId: testUserId,
        };
        let response = await posts.AddLiketoPost(postData, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
    });

    after(`Delete registered user by ID`, async () => {
        let response = await users.deleteUserById(testUserId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 5000);
    });
});
