import { ApiRequest } from "../request";
const baseUrl = global.appConfig.baseUrl;

export class RegisterController {
    async registration(emailValue: string, passwordValue: string, userNameValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                email: emailValue,
                userName: userNameValue,
                password: passwordValue
              })
            .send();
        return response;
    }
}