import { ApiRequest } from "../request";
const baseUrl = global.appConfig.baseUrl;

export class AuthController {
    async login(emailValue: string, passwordValue: string, bearerToken?: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .bearerToken(bearerToken)
            .send();
        return response;
    }
}
