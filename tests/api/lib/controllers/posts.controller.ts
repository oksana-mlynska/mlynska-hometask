import { ApiRequest } from "../request";

const baseUrl = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Posts`).send();
        return response;
    }

    async AddLiketoPost(PostData: object, accessToken) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(PostData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
